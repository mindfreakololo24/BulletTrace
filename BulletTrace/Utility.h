// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef UTILITY_H
#define UTILITY_H

#include <filesystem>

#include "types.h"

namespace Utility {
	std::filesystem::path current_module();
	CBulletTrace::Color player_color(std::uint16_t id);
};

#endif // UTILITY_H
