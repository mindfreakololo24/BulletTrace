// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "Utility.h"

#include <windows.h>

#include <samp/samp_pimpl.hpp>

#include "TraceConfig.h"

EXTERN_C IMAGE_DOS_HEADER __ImageBase;

std::filesystem::path Utility::current_module() {
	wchar_t module_name[MAX_PATH];
	GetModuleFileNameW( (HMODULE)&__ImageBase, module_name, MAX_PATH );
	return module_name;
}

CBulletTrace::Color Utility::player_color( std::uint16_t id ) {
	typedef std::uint32_t( __stdcall * get_remote_color_t )( std::uint16_t id );
	typedef std::uint32_t ( *get_loacal_color_t )();

	std::uintptr_t remote_addr = 0x0;
	std::uintptr_t local_addr = 0x0;

	switch ( SAMP::Version() ) {
// 		using enum SAMP::eVerCode;
		case SAMP::eVerCode::R1:
			remote_addr = 0xAD570;
			local_addr = 0x3D90;
			break;
		case SAMP::eVerCode::R2:
			remote_addr = 0xAD740;
			local_addr = 0x3DA0;
			break;
		case SAMP::eVerCode::R3:
			remote_addr = 0xA6AF0;
			local_addr = 0x3DA0;
			break;
		case SAMP::eVerCode::R4:
			remote_addr = 0xA7240;
			local_addr = 0x3F10;
			break;
		case SAMP::eVerCode::DL:
			remote_addr = 0xA6F70;
			local_addr = 0x3E20;
			break;
		default:
			break;
	}

	if ( !remote_addr || !local_addr ) return TraceConfig::get().GetWeaponColor( 255 );

	remote_addr += SAMP::Library();
	local_addr += SAMP::Library();

	std::uint32_t samp_color;
	if ( id != 0xFFFF )
		samp_color = ( get_remote_color_t( remote_addr ) )( id );
	else
		samp_color = ( get_loacal_color_t( local_addr ) )();

	auto color = (std::uint8_t *)&samp_color;

	return { color[2], color[1], color[0] };
}
