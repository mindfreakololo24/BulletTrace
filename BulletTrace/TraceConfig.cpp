// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TraceConfig.h"

#include <fstream>

#include "TracePool.h"
#include "Utility.h"
#include "Log.h"

TraceConfig::TraceConfig() {
	auto [path, name] = GetConfigPath();

	std::ifstream config( path / ( name.string() + ".json" ) );

	if ( !config.is_open() ) {
		Log::Info( "Config not found" );
		SetDefaultConfig();
		return;
	}

	nlohmann::json json;

	try {
		config >> json;
	} catch ( nlohmann::json::exception &e ) {
		SetDefaultConfig();
		return;
	}

	try {
		auto weapon_colors = json["weapon"];
		ReadDefaultConfig( weapon_colors );
		ReadWeaponConfig( weapon_colors );
	} catch ( ... ) { SetDefaultConfig(); }

	colorMode_ = json.value<eColorMode>( "color_mode", eColorMode::weapon_color );
	poolSize_ = json.value<std::uint32_t>( "pool_size", TracePool::kDefaultTraceCount );
	overrideOldern_ = json.value<bool>( "override_oldern", false );
	longTracers_ = json.value<bool>( "long_tracers", false );
	Log::Info( "Config is readed" );
}

TraceConfig::~TraceConfig() {
	auto [path, name] = GetConfigPath();

	std::ofstream config( path / ( name.string() + ".json" ) );

	if ( !config.is_open() ) {
		Log::Error( "Can't open config file for writing" );
		return;
	}

	nlohmann::json weapon_colors;
	WriteDefaultConfig( weapon_colors );
	WriteWeaponConfig( weapon_colors );

	nlohmann::json json;
	json["weapon"] = weapon_colors;
	json["color_mode"] = colorMode_;
	json["pool_size"] = poolSize_;
	json["override_oldern"] = overrideOldern_;
	json["long_tracers"] = longTracers_;

	try {
		config << std::setw( 2 ) << json;
	} catch ( nlohmann::json::exception &e ) { Log::Error( "Can't save config - {}", e.what() ); }
}

TraceConfig &TraceConfig::get() {
	static TraceConfig self;
	return self;
}

CBulletTrace::Color TraceConfig::GetWeaponColor( int weapon_id ) {
	if ( weapon_id > kFirstWeaponId && weapon_id < kFirstWeaponId + kWeaponCount ) return weaponTrce_[weapon_id - kFirstWeaponId].color;

	if ( weapon_id == kMinigunId ) return minigunTrace_.color;
	if ( weapon_id == kVehicleId ) return vehicleTrace_.color;

	return defaultTrace_.color;
}

void TraceConfig::SetWeaponColor( int weapon_id, CBulletTrace::Color color ) {
	if ( weapon_id > kFirstWeaponId && weapon_id < kFirstWeaponId + kWeaponCount )
		weaponTrce_[weapon_id - kFirstWeaponId].color = color;
	else if ( weapon_id == kMinigunId )
		minigunTrace_.color = color;
	else if ( weapon_id == kVehicleId )
		vehicleTrace_.color = color;
}

void TraceConfig::SetDefaultColor( CBulletTrace::Color color ) {
	defaultTrace_.color = color;
}

std::uint32_t TraceConfig::GetWeaponLifeTime( int weapon_id ) {
	if ( weapon_id > kFirstWeaponId && weapon_id < kFirstWeaponId + kWeaponCount ) return weaponTrce_[weapon_id - kFirstWeaponId].lifeTime;

	if ( weapon_id == kMinigunId ) return minigunTrace_.lifeTime;
	if ( weapon_id == kVehicleId ) return vehicleTrace_.lifeTime;

	return defaultTrace_.lifeTime;
}

void TraceConfig::SetWeaponLifeTime( int weapon_id, std::uint32_t lifeTime ) {
	if ( weapon_id > kFirstWeaponId && weapon_id < kFirstWeaponId + kWeaponCount )
		weaponTrce_[weapon_id - kFirstWeaponId].lifeTime = lifeTime;
	else if ( weapon_id == kMinigunId )
		minigunTrace_.lifeTime = lifeTime;
	else if ( weapon_id == kVehicleId )
		vehicleTrace_.lifeTime = lifeTime;
}

void TraceConfig::SetDefaultLifeTime( std::uint32_t lifeTime ) {
	defaultTrace_.lifeTime = lifeTime;
}

float TraceConfig::GetWeaponRadius( int weapon_id ) {
	if ( weapon_id > kFirstWeaponId && weapon_id < kFirstWeaponId + kWeaponCount ) return weaponTrce_[weapon_id - kFirstWeaponId].radius;

	if ( weapon_id == kMinigunId ) return minigunTrace_.radius;
	if ( weapon_id == kVehicleId ) return vehicleTrace_.radius;

	return defaultTrace_.radius;
}

void TraceConfig::SetWeaponRadius( int weapon_id, float radius ) {
	if ( weapon_id > kFirstWeaponId && weapon_id < kFirstWeaponId + kWeaponCount )
		weaponTrce_[weapon_id - kFirstWeaponId].radius = radius;
	else if ( weapon_id == kMinigunId )
		minigunTrace_.radius = radius;
	else if ( weapon_id == kVehicleId )
		vehicleTrace_.radius = radius;
}

void TraceConfig::SetDefaultRadius( float radius ) {
	defaultTrace_.radius = radius;
}

eColorMode &TraceConfig::ColorMode() {
	return colorMode_;
}

std::uint32_t TraceConfig::GetPoolSize() {
	return poolSize_;
}

void TraceConfig::SetPoolSize( std::uint32_t size ) {
	poolSize_ = size;
	TracePool::get().resize( size );
}

bool &TraceConfig::OverrideOldern() {
	return overrideOldern_;
}

bool &TraceConfig::LongTracers() {
	return longTracers_;
}

std::pair<std::filesystem::path, std::filesystem::path> TraceConfig::GetConfigPath() {
	auto module_path = Utility::current_module();
	auto path = std::filesystem::current_path();
	if ( module_path.has_parent_path() ) path = module_path.parent_path();
	std::filesystem::path name = "BulletTrace";
	if ( module_path.has_stem() ) name = module_path.stem();

	return { path, name };
}

CBulletTrace::Color TraceConfig::DefaultGameColor() const {
	std::uint32_t colorBGRA = kDefaultTraceColor;
	if ( *(std::uint8_t *)kColorInstructionAddress == kColorInstructionValue )
		colorBGRA = *(std::uint32_t *)( kColorInstructionAddress + 1 );

	auto color = (std::uint8_t *)&colorBGRA;
	return { color[1], color[2], color[3] };
}

std::uint32_t TraceConfig::DefaultGameLifeTime() const {
	std::uint32_t result{ kDefaultTraceLifeTime };
	if ( *(std::uint8_t *)kLifeTimeInstructionAddress == kLifeTimeInstructionValue )
		result = *(std::uint32_t *)( kLifeTimeInstructionAddress + 1 );

	return result;
}

float TraceConfig::DefaultGameRadius() const {
	float result{ kDefaultTraceRadius };
	if ( *(std::uint8_t *)kRadiusInstructionAddress == kRadiusInstructionValue ) result = *(float *)( kRadiusInstructionAddress + 1 );

	return result;
}

void TraceConfig::ReadDefaultConfig( nlohmann::json &json ) {
	try {
		auto def_config = json["default"];
		def_config.get_to( defaultTrace_ );
	} catch ( ... ) {
		defaultTrace_.color = DefaultGameColor();
		defaultTrace_.lifeTime = DefaultGameLifeTime();
		defaultTrace_.radius = DefaultGameLifeTime();
	}
}

void TraceConfig::ReadWeaponConfig( nlohmann::json &json ) {
	for ( auto i = kFirstWeaponId; i < kFirstWeaponId + kWeaponCount; ++i ) {
		try {
			auto config = json[std::to_string( i )];
			config.get_to( weaponTrce_[i - kFirstWeaponId] );
		} catch ( ... ) { weaponTrce_[i - kFirstWeaponId] = defaultTrace_; }
	}

	try {
		auto config = json[kMinigunIdStr];
		config.get_to( minigunTrace_ );
	} catch ( ... ) { minigunTrace_ = defaultTrace_; }
	try {
		auto config = json[kVehicleIdStr];
		config.get_to( vehicleTrace_ );
	} catch ( ... ) { vehicleTrace_ = defaultTrace_; }
}

void TraceConfig::WriteDefaultConfig( nlohmann::json &json ) {
	json["default"] = defaultTrace_;
}

void TraceConfig::WriteWeaponConfig( nlohmann::json &json ) {
	for ( auto i = kFirstWeaponId; i < kFirstWeaponId + kWeaponCount; ++i ) { json[std::to_string( i )] = weaponTrce_[i - kFirstWeaponId]; }
	json[kMinigunIdStr] = minigunTrace_;
	json[kVehicleIdStr] = vehicleTrace_;
}

void TraceConfig::SetDefaultConfig() {
	defaultTrace_.color = DefaultGameColor();
	defaultTrace_.lifeTime = DefaultGameLifeTime();
	defaultTrace_.radius = DefaultGameRadius();
	for ( auto i = kFirstWeaponId; i < kFirstWeaponId + kWeaponCount; ++i ) { weaponTrce_[i - kFirstWeaponId] = defaultTrace_; }
	minigunTrace_ = defaultTrace_;
	vehicleTrace_ = defaultTrace_;

	colorMode_ = eColorMode::weapon_color;
	poolSize_ = TracePool::kDefaultTraceCount;
	overrideOldern_ = false;
}

void from_json( const nlohmann::json &j, TraceConfig::weapon_config_t &config ) {
	config.color = j.value<CBulletTrace::Color>( "color", { 0xFF, 0xFF, 0x00 } );
	config.lifeTime = j.value<std::uint32_t>( "life_time", 300 );
	config.radius = j.value<float>( "radius", 0.01f );
}

void to_json( nlohmann::json &j, const TraceConfig::weapon_config_t &config ) {
	j["color"] = config.color;
	j["life_time"] = config.lifeTime;
	j["radius"] = config.radius;
}
