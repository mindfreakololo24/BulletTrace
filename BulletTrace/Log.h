// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef LOG_H
#define LOG_H

#include <fstream>

#include <fmt/include/fmt/core.h>

namespace Log {
	namespace details {
		std::ostream &log_stream();
		template<typename Fmt, typename... Args> inline void print( std::string_view prefix, const Fmt &fmt, Args &&...args ) {
            details::log_stream() << prefix << fmt::format( fmt::runtime(fmt), args... ) << std::endl;
		}
	} // namespace details

	template<typename Fmt, typename... Args> void Info( const Fmt &fmt, Args &&...args ) { details::print( "Info: ", fmt, args... ); }

	template<typename Fmt, typename... Args> void Error( const Fmt &fmt, Args &&...args ) { details::print( "Error: ", fmt, args... ); }
}; // namespace Log

#endif // LOG_H
