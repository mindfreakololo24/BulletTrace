// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACEADD_H
#define TRACEADD_H

#include <llmo/SRHook.hpp>

class TraceAdd {
	static constexpr auto kDefaultTraceColor = 0x00'FF'FF'80;
	static constexpr auto kColorInstructionAddress = 0x723CBD;
	static constexpr auto kColorInstructionValue = 0xBF;

	SRHook::Hook<> writeFields_{ 0x7238F7, 5 };
	SRHook::Hook<> unknownTrace_{ 0x726B28, 6 };
	SRHook::Hook<> unknownTrace2_{ 0x726B14, 6 };
	SRHook::Hook<class RwV3D*, class RwV3D*, std::uint32_t, std::uint8_t> addTrace_{ 0x726D2B, 5 };
	SRHook::Hook<> sampBulletSender_{ 0x984D, 6, "samp" };

public:
	TraceAdd();

protected:
	std::uint8_t weaponId_ = 255;
	std::uint16_t playerId_ = 0xFFFF;
	void WriteCustomFields(SRHook::CPU &cpu);
	void UnknownTrace();
	void AddTrace( SRHook::CPU &cpu, class RwV3D*&from, class RwV3D*&to, std::uint32_t &lifeTime, std::uint8_t &alpha );
	void GetPlayerId( SRHook::CPU &cpu );
};

#endif // TRACEADD_H

