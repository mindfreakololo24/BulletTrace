// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACECONFIG_H
#define TRACECONFIG_H

#include <filesystem>

#include "types.h"
#include "TraceAPI.h"

class TraceConfig {
	static constexpr auto kDefaultTraceColor = 0x00'FF'FF'80;
	static constexpr auto kColorInstructionAddress = 0x723CBD;
	static constexpr auto kColorInstructionValue = 0xBF;
	static constexpr auto kDefaultTraceLifeTime = 300;
	static constexpr auto kLifeTimeInstructionAddress = 0x726CE1;
	static constexpr auto kLifeTimeInstructionValue = 0x68;
	static constexpr auto kDefaultTraceRadius = 0.01f;
	static constexpr auto kRadiusInstructionAddress = 0x726CEA;
	static constexpr auto kRadiusInstructionValue = 0x68;

	static constexpr auto kMinigunId = 38;
	static constexpr auto kMinigunIdStr = "38";
	static constexpr auto kVehicleId = 49;
	static constexpr auto kVehicleIdStr = "49";
	static constexpr auto kFirstWeaponId = 22;
	static constexpr auto kWeaponCount = 12;

	TraceConfig();
	~TraceConfig();

public:
	struct weapon_config_t {
		CBulletTrace::Color color;
		std::uint32_t lifeTime;
		float radius;
	};

	static TraceConfig &get();

	CBulletTrace::Color GetWeaponColor( int weapon_id );
	void SetWeaponColor( int weapon_id, CBulletTrace::Color color );
	void SetDefaultColor( CBulletTrace::Color color );

	std::uint32_t GetWeaponLifeTime( int weapon_id );
	void SetWeaponLifeTime( int weapon_id, std::uint32_t lifeTime );
	void SetDefaultLifeTime( std::uint32_t lifeTime );

	float GetWeaponRadius( int weapon_id );
	void SetWeaponRadius( int weapon_id, float radius );
	void SetDefaultRadius( float radius );

	eColorMode &ColorMode();

	std::uint32_t GetPoolSize();
	void SetPoolSize( std::uint32_t size );

	bool &OverrideOldern();

	bool &LongTracers();

protected:
	weapon_config_t weaponTrce_[kWeaponCount];
	weapon_config_t minigunTrace_;
	weapon_config_t vehicleTrace_;
	weapon_config_t defaultTrace_;

	eColorMode colorMode_;
	std::uint32_t poolSize_;
	bool overrideOldern_;
	bool longTracers_;

private:
	std::pair<std::filesystem::path, std::filesystem::path> GetConfigPath();
	CBulletTrace::Color DefaultGameColor() const;
	std::uint32_t DefaultGameLifeTime() const;
	float DefaultGameRadius() const;

	void ReadDefaultConfig( nlohmann::json &json );
	void ReadWeaponConfig( nlohmann::json &json );
	void WriteDefaultConfig( nlohmann::json &json );
	void WriteWeaponConfig( nlohmann::json &json );

	void SetDefaultConfig();
};

void from_json( const nlohmann::json &j, TraceConfig::weapon_config_t &config );
void to_json( nlohmann::json &j, const TraceConfig::weapon_config_t &config );

#endif // TRACECONFIG_H
