// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TraceAPI.h"

#include "TraceConfig.h"

TraceConfig &GetConfig() {
	return TraceConfig::get();
}

eColorMode GetColorMode() {
	return TraceConfig::get().ColorMode();
}

void SetColorMode( eColorMode mode ) {
	TraceConfig::get().ColorMode() = mode;
}

bool GetLongTracers() {
	return TraceConfig::get().LongTracers();
}

void SetLongTracers( bool long_tracers ) {
	TraceConfig::get().LongTracers() = long_tracers;
}

bool GetOverrideOldern() {
	return TraceConfig::get().OverrideOldern();
}

void SetOverrideOldern( bool override_oldern ) {
	TraceConfig::get().OverrideOldern() = override_oldern;
}

int GetPoolSize() {
	return TraceConfig::get().GetPoolSize();
}

void SetPoolSize( int size ) {
	TraceConfig::get().SetPoolSize( size );
}

unsigned GetDefaultColor() {
	return GetWeaponColor( 255 );
}

void SetDefaultColor( unsigned colorBGRA ) {
	auto color = (std::uint8_t *)&colorBGRA;
	TraceConfig::get().SetDefaultColor( { color[1], color[2], color[3] } );
}

unsigned GetWeaponColor( int weapon_id ) {
	auto color = TraceConfig::get().GetWeaponColor( weapon_id );
	return ( 0x80 << 24 ) | ( color.red << 16 ) | ( color.green << 8 ) | color.blue;
}

void SetWeaponColor( int weapon_id, unsigned colorBGRA ) {
	auto color = (std::uint8_t *)&colorBGRA;
	TraceConfig::get().SetWeaponColor( weapon_id, { color[1], color[2], color[3] } );
}

unsigned GetDefaultLifeTime() {
	return GetWeaponLifeTime( 255 );
}

void SetDefaultLifeTime( unsigned life_time ) {
	TraceConfig::get().SetDefaultLifeTime( life_time );
}

unsigned GetWeaponLifeTime( int weapon_id ) {
	return TraceConfig::get().GetWeaponLifeTime( weapon_id );
}

void SetWeaponLifeTime( int weapon_id, unsigned life_time ) {
	TraceConfig::get().SetWeaponLifeTime( weapon_id, life_time );
}

float GetDefaultRadius() {
	return GetWeaponRadius( 255 );
}

void SetDefaultRadius( float radius ) {
	TraceConfig::get().SetDefaultRadius( radius );
}

float GetWeaponRadius( int radius ) {
	return TraceConfig::get().GetWeaponRadius( radius );
}

void SetWeaponRadius( int weapon_id, float radius ) {
	TraceConfig::get().SetWeaponRadius( weapon_id, radius );
}
