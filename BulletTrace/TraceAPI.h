// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACE_API_H
#define TRACE_API_H

#ifdef EXPORT_API_SYMBOLS
#	define TRACE_API __declspec( dllexport )
#else
#	define TRACE_API __declspec( dllimport )
#endif


#if __cplusplus
class TraceConfig &TRACE_API GetConfig();
enum class eColorMode {
#else
enum eColorMode {
#endif
	weapon_color,
	nick_color
};

#if __cplusplus
extern "C" {
#endif
eColorMode TRACE_API GetColorMode();
void TRACE_API SetColorMode( eColorMode mode );

bool TRACE_API GetLongTracers();
void TRACE_API SetLongTracers( bool long_tracers );

bool TRACE_API GetOverrideOldern();
void TRACE_API SetOverrideOldern( bool override_oldern );

int TRACE_API GetPoolSize();
void TRACE_API SetPoolSize( int size );

unsigned TRACE_API GetDefaultColor();
void TRACE_API SetDefaultColor( unsigned colorBGRA );

unsigned TRACE_API GetWeaponColor( int weapon_id );
void TRACE_API SetWeaponColor( int weapon_id, unsigned colorBGRA );

unsigned TRACE_API GetDefaultLifeTime();
void TRACE_API SetDefaultLifeTime( unsigned life_time );

unsigned TRACE_API GetWeaponLifeTime( int weapon_id );
void TRACE_API SetWeaponLifeTime( int weapon_id, unsigned life_time );

float TRACE_API GetDefaultRadius();
void TRACE_API SetDefaultRadius( float radius );

float TRACE_API GetWeaponRadius( int weapon_id );
void TRACE_API SetWeaponRadius( int weapon_id, float radius );
#if __cplusplus
}
#endif

#endif // TRACE_API_H
