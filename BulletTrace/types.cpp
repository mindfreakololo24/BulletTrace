#include "types.h"

void from_json( const nlohmann::json &j, CBulletTrace::Color &color ) {
	color.red = j.value<std::uint8_t>( "red", 0xFF );
	color.green = j.value<std::uint8_t>( "green", 0xFF );
	color.blue = j.value<std::uint8_t>( "blue", 0x00 );
}

void to_json( nlohmann::json &j, const CBulletTrace::Color &color ) {
	j["red"] = color.red;
	j["green"] = color.green;
	j["blue"] = color.blue;
}
