// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TraceExtend.h"

#include "TracePool.h"
#include "TraceConfig.h"
#include "Log.h"

TraceExtend::TraceExtend() {
	updatePoolPtr_.onAfter += std::tuple{ this, &TraceExtend::GetUpdatePoolPtr };
	updatePoolPtr_.install( 0, 0, false );
	updatePoolSize_.onBefore += std::tuple{ this, &TraceExtend::GetUpdatePoolSize };
	updatePoolSize_.install( 0, 0, true );
	addTracePoolPtr_.onBefore += std::tuple{ this, &TraceExtend::GetAddTracePoolPtr };
	addTracePoolPtr_.install( 0, 0, false );
	addTracePoolSize_.onAfter += std::tuple{ this, &TraceExtend::GetAddTracePoolSize };
	addTracePoolSize_.install( 0, 0, true );
	addTraceInsert_.onBefore += std::tuple{ this, &TraceExtend::GetAddTraceInsert };
	addTraceInsert_.install( 0x2C, 0, false );
}

void TraceExtend::GetUpdatePoolPtr( SRHook::Info &info ) {
	info.cpu.EAX = std::uintptr_t( &TracePool::get().pool()->m_nLifeTime );

	info.retAddr = 0x723FBD;
}

void TraceExtend::GetUpdatePoolSize( SRHook::CPU &cpu ) {
	if ( cpu.EAX < std::uintptr_t( &TracePool::get().pool()->m_nLifeTime ) + sizeof( CBulletTrace ) * TracePool::get().size() )
		cpu.SF = !cpu.OF;
	else
		cpu.SF = cpu.OF;
}

void TraceExtend::GetAddTracePoolPtr( SRHook::CPU &cpu ) {
	cpu.EAX = std::uintptr_t( &TracePool::get()[1]->m_bExists );
}

void TraceExtend::GetAddTracePoolSize( SRHook::Info &info ) {
	if ( info.cpu.EAX <
		 std::uintptr_t( TracePool::get().pool() ) + sizeof( CBulletTrace ) * TracePool::get().size() + TracePool::kTracePoolOffset )
		info.cpu.SF = !info.cpu.OF;
	else
		info.cpu.SF = info.cpu.OF;

	info.retAddr = 0x7237B4;
}

void TraceExtend::GetAddTraceInsert( SRHook::Info &info,
									 RwV3D *&pStartPoint,
									 RwV3D *&pEndPoint,
									 float &radius,
									 unsigned int &dissapearTime,
									 char &alpha ) {
	info.skipOriginal = true;
	auto traceId = ( !TracePool::get()[nextTraceId_]->m_bExists ? nextTraceId_ : TracePool::get().size() );
	auto oldernTraceId = TracePool::get().size();
	auto m_snTimeInMilliseconds = *(std::uint32_t *)0xB7CB84;

	if ( traceId == TracePool::get().size() ) {
		for ( std::uint32_t i = 0, oldern = 0; i < TracePool::get().size(); ++i ) {
			auto trace = TracePool::get()[traceId];
			if ( !trace->m_bExists ) {
				traceId = i;
				break;
			}
			if ( !TraceConfig::get().OverrideOldern() ) continue;
			auto expireTime = trace->m_nCreationTime + trace->m_nLifeTime;
			if ( expireTime < oldern || !oldern ) {
				oldernTraceId = i;
				oldern = expireTime;
			}
		}

		if ( traceId == TracePool::get().size() && oldernTraceId != TracePool::get().size() && TraceConfig::get().OverrideOldern() )
			traceId = oldernTraceId;
	}

	if ( traceId != TracePool::get().size() ) {
		nextTraceId_ = traceId + 1;
		if ( nextTraceId_ == TracePool::get().size() ) nextTraceId_ = 0;

		auto trace = TracePool::get()[traceId];
		trace->m_bExists = true;
		trace->m_vecStart = *pStartPoint;
		trace->m_vecEnd = *pEndPoint;
		trace->m_fRadius = radius;
		trace->m_nLifeTime = dissapearTime;
		trace->m_nTransparency = alpha;
		trace->m_nCreationTime = m_snTimeInMilliseconds;
	}

	info.cpu.ECX = traceId * sizeof( CBulletTrace );
	info.retAddr = 0x7238F7;
}
