// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACERENDER_H
#define TRACERENDER_H

#include <llmo/SRHook.hpp>

class TraceRender {
	SRHook::Hook<> renderPtr_{ 0x723CB2, 6 };
	SRHook::Hook<> renderColor_{ 0x723D1D, 5 };
	SRHook::Hook<> renderCount_{ 0x723F5D, 6 };

public:
	TraceRender();

protected:
	void GetRenderPtr( SRHook::Info &info );
	void GetRenderColor( SRHook::CPU &cpu );
	void GetRenderCount( SRHook::CPU &cpu );
};

#endif // TRACERENDER_H
