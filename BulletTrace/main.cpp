#include "main.h"

#include "TraceRender.h"
#include "TraceAdd.h"
#include "TraceExtend.h"
#include "TraceConfig.h"

AsiPlugin::AsiPlugin() : SRDescent( nullptr ) {
	// Constructor

	TraceConfig::get(); // Initialize config before hook installation

	render_ = std::make_unique<TraceRender>();
	add_ = std::make_unique<TraceAdd>();
	extend_ = std::make_unique<TraceExtend>();
}

AsiPlugin::~AsiPlugin() {
	// Destructor
}
