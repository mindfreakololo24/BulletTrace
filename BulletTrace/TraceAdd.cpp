// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#include "TraceAdd.h"

#include <gtasa/CGame/CPed.h>
#include <samp/samp_pimpl.hpp>

#include "TracePool.h"
#include "TraceConfig.h"
#include "Log.h"
#include "Utility.h"

TraceAdd::TraceAdd() {
	writeFields_.onBefore += std::tuple{ this, &TraceAdd::WriteCustomFields };
	writeFields_.install( 0, 0, false );
	unknownTrace_.onBefore += std::tuple{ this, &TraceAdd::UnknownTrace };
	unknownTrace_.install( 0, 0, true );
	unknownTrace2_.onBefore += std::tuple{ this, &TraceAdd::UnknownTrace };
	unknownTrace2_.install( 0, 0, true );
	addTrace_.onBefore += std::tuple{ this, &TraceAdd::AddTrace };
	addTrace_.install( 0, 0, false );

	if ( SAMP::isR1() )
		sampBulletSender_.changeAddr( 0x96CD );
	else if ( SAMP::isR2() )
		sampBulletSender_.changeAddr( 0x96D1 );
	else if ( SAMP::isR4() ) // -1
		sampBulletSender_.changeAddr( 0x9BB1 );
	else if ( SAMP::isDL() )
		sampBulletSender_.changeAddr( 0x98B1 );
	sampBulletSender_.onBefore += std::tuple{ this, &TraceAdd::GetPlayerId };
	sampBulletSender_.install( 0, 0, false );
}

void TraceAdd::WriteCustomFields( SRHook::CPU &cpu ) {
	auto traceId = cpu.ECX / sizeof( CBulletTrace );
	if ( traceId >= TracePool::get().size() ) {
		Log::Error( "Invalid trace id {}/{}", traceId, TracePool::get().size() );
		return;
	}

	auto trace = TracePool::get()[traceId];

	trace->weapon_id = weaponId_;
	trace->player_id = playerId_;
	switch ( TraceConfig::get().ColorMode() ) {
			// 		using enum eColorMode;
		case eColorMode::weapon_color:
			trace->color = TraceConfig::get().GetWeaponColor( weaponId_ );
			break;
		case eColorMode::nick_color:
			trace->color = Utility::player_color( playerId_ );
			break;
	}

	trace->m_nLifeTime = TraceConfig::get().GetWeaponLifeTime( weaponId_ );
	trace->m_fRadius = TraceConfig::get().GetWeaponRadius( weaponId_ );

	Log::Info( "Add trace {}/{} for weapon {}", traceId, TracePool::get().size(), weaponId_ );
	weaponId_ = 255;
	playerId_ = 0xFFFF;
}

void TraceAdd::UnknownTrace() {
	weaponId_ = 255;
}

void TraceAdd::AddTrace( SRHook::CPU &cpu, class RwV3D *&from, class RwV3D *&to, std::uint32_t &lifeTime, std::uint8_t &alpha ) {
	weaponId_ = *(int *)( cpu.ESP + 0x4C + 0xC );

	if ( TraceConfig::get().LongTracers() ) {
		*from = **(RwV3D **)( cpu.ESP + 0x4C + 0x4 );
		*to = **(RwV3D **)( cpu.ESP + 0x4C + 0x8 );
	}
}

void TraceAdd::GetPlayerId( SRHook::CPU &cpu ) {
	playerId_ = *(std::uint16_t *)( cpu.ESP + 0x8 );
}
