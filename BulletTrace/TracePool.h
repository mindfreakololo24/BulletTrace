// <one line to give the program's name and a brief idea of what it does.>
// SPDX-FileCopyrightText: 2022 SR_team <me@sr.team>
// SPDX-License-Identifier: GPL-2.0

#ifndef TRACEPOOL_H
#define TRACEPOOL_H

#include "types.h"

struct TracePool_t {
	CBulletTrace *pool = nullptr;
	std::size_t size = 0;
};

class TracePool {
	static constexpr auto kCMPInstructionAddress = 0x723F57;
	static constexpr auto kCMPInstructionCode = 0xFE81;
	static constexpr auto kTracePoolPointerAddress = 0x723756;
	static constexpr auto kTracePoolPointerInstructionAddress = 0x723755;
	static constexpr auto kTracePoolPointerInstructionCode = 0xB8;
	static constexpr auto kTracePoolAddress = 0xC7C748;

	TracePool();
	TracePool( TracePool & ) = delete;
	TracePool( TracePool && ) = delete;
	~TracePool() = default;

    TracePool_t pool_;

public:
	static constexpr auto kDefaultTraceCount = 16;
	static constexpr auto kTracePoolOffset = 0x44;

	static TracePool &get();

	CBulletTrace *pool() const;
	std::size_t size() const;

	CBulletTrace *operator[]( std::uint32_t id ) const;

	void resize( std::uint32_t size );
};

#endif // TRACEPOOL_H
